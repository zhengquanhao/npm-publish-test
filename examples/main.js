import Vue from 'vue'
import App from './App.vue'
import Header from '../packages/index'

Vue.use(Header)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
