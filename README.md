# 头部npm包-Vue版

## 安装

``` bash
$ npm install ron-npm-template -S
```
## 使用

在 `main.js` 文件中引入插件并注册

``` bash
# main.js
import ronNpmTemplate from 'ron-npm-template'
import 'ron-npm-template/lib/ron-npm-template.css'
Vue.use(ronNpmTemplate)
```

在项目中用使用 ronNpmTemplate


```js
<template>
  <ron-npm-template />
</template>
```