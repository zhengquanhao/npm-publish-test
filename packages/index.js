import Header from './header/main.vue'

Header.install = function (Vue) {
  Vue.component(Header.name, Header)
}

export default Header
